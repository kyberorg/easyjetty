package virtalab.easyjetty.app;

import org.eclipse.jetty.server.Handler;

import virtalab.easyjetty.core.handlers.CoreHandler;
import virtalab.easyjetty.core.handlers.JettyServletHandler;
import virtalab.sys.Shell;

public class App {

	public static void init(){
		Conf.init();
		CoreHandler.init();
	}

	public static void sayGoodByeOnExit(){
		GoodByeHook hook = new GoodByeHook();
		Runtime.getRuntime().addShutdownHook(hook);
	}

	public static String getHandlerName(Handler handler){
		String handlerName = null;
		if(handler instanceof JettyServletHandler){ handlerName = ((JettyServletHandler) handler).getName(); }
		if(handlerName == null) {handlerName = handler.getClass().getSimpleName(); }
		return handlerName;
	}

	//HOOK
	private static class GoodByeHook extends Thread {
		@Override
		public void run(){
			Shell.echo("Web Server shutting down. Bye-bye.");
		}
	}
}
