package virtalab.easyjetty.app.util;

import virtalab.sys.Shell;

public class Help {
	//TODO improve help section
	public static boolean isHelpFlagDetected(String[] args){
		if(args.length>=1){
			return (args[0].equals("-h") || args[0].equals("--help"));
		} else {
			//no args -> no help
			return false;
		}
	}

	public static void showHelp(){

		String help0 = "EasyJetty help";

		StringBuffer arg = new StringBuffer();
		arg.append("Valid Arguments are: "+S.lt);
		arg.append("config file - Absolute or relative path to conf file. Should end with .conf"+S.lt);
		arg.append("port -  Valid TCP Port. "+S.lt);
		arg.append("host - Hostname or IP address. Hostname should be resovleable to ip. IP address should be present at this machine."+S.lt);
		arg.append("--help | -h - Show this help section.");

		StringBuffer opt = new StringBuffer();
		opt.append("Valid VM options are: "+S.lt);
		opt.append("web.config - location of Web Server Configuration file"+S.lt);
		opt.append("virtalab.LEVEL=<log-level> - set required log level"+S.lt);

		StringBuffer llvl = new StringBuffer();
		llvl.append("Valid log-levels: "+S.lt);
		llvl.append("DEBUG"+S.lt);
		llvl.append("INFO"+S.lt);
		llvl.append("WARN"+S.lt);

		Shell.echo(help0);
		Shell.echo("");
		showUsageMessage();
		Shell.echo("");
		Shell.echo(arg.toString());
		Shell.echo("");
		Shell.echo(opt.toString());
		//Shell.echo("");
		Shell.echo(llvl.toString());

	}

	private static void showUsageMessage(){
		String usage0 = "Usage: java -D<valid-option>=<value> -jar <easyjetty-app>.jar ";
		String usage1 = "Usage: java -D<valid-option>=<value> -jar <easyjetty-app>.jar <arguments>";

		Shell.echo(usage0);
		Shell.echo(usage1);
	}

}
