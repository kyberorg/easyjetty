package virta.easyjetty.util;

import virta.easyjetty.logger.AccessLogger;
import virta.easyjetty.logger.ErrorLogger;

public class LoggerTuner {

/**
 * Setup access logger
 *  
 * @param filename Access log filename 
 */
public static AccessLogger setupAccessLogger(String filename){
	//TODO set filename from config
	AccessLogger accessLog = new AccessLogger(filename);
	accessLog.setAppend(true);
	accessLog.setExtended(true);
	//TODO get from config or 0 - as default
	accessLog.setRetainDays(0);
	//TODO get from config or UTC (default)
	accessLog.setLogTimeZone("UTC");
	
	return accessLog;
}

/**
 * Setup error logger
 *  
 * @param filename Error log filename 
 */
public static ErrorLogger setupErrorLogger(String filename){
	//TODO set filename from config
	ErrorLogger errorLog = new ErrorLogger(filename);
	errorLog.setAppend(true);
	errorLog.setExtended(true);
	//TODO get from config or 0 - as default
	errorLog.setRetainDays(0);
	//TODO get from config or UTC (default)
	errorLog.setLogTimeZone("UTC");
	
	return errorLog;
}
}
