package virta.easyjetty.struct;

public class ErrorPageStruct {
	private String webDesc;
	private String shortDesc;
	private String longDesc;

	public String getWebDesc() {
		return webDesc;
	}
	/**
	 * Set Standard Web Error
	 * 
	 * @param webDesc
	 */
	public void setWebDesc(String webDesc) {
		this.webDesc = webDesc;
	}

	public String getShortDesc() {
		return shortDesc;
	}
	/**
	 * Short Description for User
	 * @param shortDesc
	 */
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getLongDesc() {
		return longDesc;
	}
	/**
	 * Long Description for User
	 * 
	 * @param longDesc
	 */
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	
}
