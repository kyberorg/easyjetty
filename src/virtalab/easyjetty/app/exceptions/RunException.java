package virtalab.easyjetty.app.exceptions;

public class RunException extends WebServerException {
	private static final long serialVersionUID = 879554835701887702L;

	public RunException(Throwable e){
		super(e);
	}
}
