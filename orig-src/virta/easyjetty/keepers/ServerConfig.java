package virta.easyjetty.keepers;

import java.util.ArrayList;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;

public class ServerConfig {
	private static ServerConfig self;
	
	private ArrayList<Connector> connectors;
	private ArrayList<Handler> handlers;

	private int port = 0;
	private String host = "0.0.0.0";
	
	private ServerConfig(){
		this.connectors = new ArrayList<Connector>();
		this.handlers = new ArrayList<Handler>();
	}
	public static ServerConfig init(){
		if(self == null){
			self = new ServerConfig();
		}
		return self;
	}
	
	public static ServerConfig getConfig(){
		return ServerConfig.init();
	}
	
	public void addConnector(Connector conn){
		connectors.add(conn);
	}
	public void addHandler(Handler handler){
		handlers.add(handler);
	}
	public void setPort(int port) {
		this.port = port;
	}
	public void setHost(String host) {
		this.host = host;
	}
	
	//methods for runner
	public ArrayList<Connector> getConnectors(){
		return connectors;
	} 
	public ArrayList<Handler> getHandlers(){
		return handlers;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getHost() {
		return host;
	}
}
