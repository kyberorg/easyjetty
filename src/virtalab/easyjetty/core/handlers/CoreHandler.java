package virtalab.easyjetty.core.handlers;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.app.Conf;
import virtalab.sys.Shell;

public class CoreHandler extends AbstractHandler {
	private static Logger log = Log.getLogger(CoreHandler.class);

	private static CoreHandler self;

	private final HashMap<String,Handler> handlers = new HashMap<String,Handler>();
	private final String serverBase = Conf.getConf().getServerOffset();

	private CoreHandler(){}

	public static CoreHandler getInstance(){
		if(self==null){
			self = new CoreHandler();
		}
		return self;
	}

	//alias
	public static CoreHandler init(){
		return getInstance();
	}

	@Override
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response){

			//init
			Handler handler;

			try{
			log.debug("NEW REQUEST");
			log.debug("full target "+target);
			String key,newTarget;
			//as we need only part of URL, we are about to parse it
			String[] tgt = target.split("/");
			if(tgt.length>1){
				key = "/"+tgt[1];
				log.debug("Seraching for key "+key);
				newTarget = target.replaceFirst("/"+tgt[1],"");
			} else {
				key = target;
				newTarget = target;
			}
			if(newTarget.equals("")){
				newTarget = "/";
			}
			log.debug("new Target is "+newTarget);
			//END of target parser

			//find key in hashMap
			if(handlers.containsKey(key)){
				//key found - send to handler
				handler = handlers.get(key);
				log.debug("Request is served by "+handler.toString());
				handler.handle(newTarget, baseRequest, request, response);
			} else if(handlers.containsKey("/")) {
				//send to default (catch-all) handler
				handler = handlers.get("/");
				log.debug("Request is served by "+handler.toString());
				handler.handle(target, baseRequest, request, response);
			} else {
				//404
				log.debug("Handler not found");
				response.setStatus(404);
				ErrorHandler.getHandler().handle(target, baseRequest, request, response);
			}
			//post actions
			}catch(Exception e){
				log.warn("Server error", e);
				log.debug(e);
				response.setStatus(500);
				ErrorHandler.getHandler().handle(target, baseRequest, request, response);
			}
			//Server header
			response.setHeader("Server", this.getServerHeader());

			if(log.isDebugEnabled()){
				Shell.echo(""); //this is just line to make whitespace between requests in debug logs
			}
			baseRequest.setHandled(true);
			}

	/**
	 * Adds new handler to collection
	 *
	 * @param handler Handler
	 * @param base Path for this handler
	 */
	public void addHandler(Handler handler, String base){
		handlers.put(base,handler);
	}

	/**
	 * Getter
	 * @return rootBase (or default base)
	 */
	public String getRootBase(){
		if(this.serverBase==null){
			//default
			return "/";
		} else {
			return this.serverBase;
		}
	}
	/**
	 * Makes serverHeader
	 * @return header values
	 */
	private String getServerHeader(){
		String token = Conf.getConf().getServerToken();
		String prod = Conf.getConf().getServerProduct();
		String ver = Conf.getConf().getServerVersion();
		String header = "";

		boolean isProd = token.equalsIgnoreCase("Prod");
		boolean isOff = token.equalsIgnoreCase("Off");
		if(isProd){
			header = prod;
			header = prod +"-" + ver;
		} else if(isOff){
			header = "Server signature is disabled";
		} else {
			//default Full
			header = prod +"-" + ver;
		}

		return header;
	}
}
