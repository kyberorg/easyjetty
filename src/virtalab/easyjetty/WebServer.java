package virtalab.easyjetty;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.app.Conf;
import virtalab.easyjetty.app.config.Defaults;
import virtalab.easyjetty.app.exceptions.ConfigurationException;
import virtalab.easyjetty.app.exceptions.NotValidInputException;
import virtalab.easyjetty.app.exceptions.RunException;
import virtalab.easyjetty.app.exceptions.WrongArgumentException;
import virtalab.easyjetty.app.util.Help;
import virtalab.easyjetty.app.util.LOG;
import virtalab.easyjetty.app.util.S;
import virtalab.easyjetty.app.validation.InputWizard;
import virtalab.easyjetty.core.connectors.HttpConnector;
import virtalab.easyjetty.core.handlers.ServletHandler;
import virtalab.easyjetty.core.loggers.AccessLogger;
import virtalab.easyjetty.core.loggers.ErrorLogger;
import virtalab.easyjetty.core.servers.CoreServer;
import virtalab.sys.Shell;

public class WebServer extends CoreServer {

	private static final Logger log = Log.getLogger(WebServer.class);

	//connector
	private final HttpConnector connector = new HttpConnector();

	//handlers spot
	private WebHandler handler = null;

	private final HashMap<String, String> servlets = new HashMap<String,String>();


	//Constructors
	public WebServer(String[] args) throws WrongArgumentException, ConfigurationException{
		this.init(args);
	}
	public WebServer() throws ConfigurationException{
		try{
			this.init(null);
		}catch(WrongArgumentException wae){
			log.debug("For this reason: "+wae.getMessage()+" we have an WrongArgumentException. Sorry...");
		}
	}

	public void init(String[] args) throws ConfigurationException, WrongArgumentException{
		//trying to load configuration file or setting defaults
		String conf = System.getProperty(Defaults.CONFIG_PARAM,"");

		if(!conf.equals("")){
			InputWizard.loadConfig(conf);
		} else {
			super.setDefaults();
		}

		//init properties
		InputWizard.PropertiesProccessor();

		if(args!=null){
			//analyze arguments
			InputWizard.ArgumentProccessor(args);

			//If first argument is help flag - showing help and exiting
			if(Help.isHelpFlagDetected(args)){
				Help.showHelp();
				System.exit(0);
			}
		}

		//create Root Handler
		ServletHandler rootHandler = new ServletHandler(Defaults.ROOT_HANDLER);
		//adding to slot "handler"
		this.handler = new WebHandler();
		this.handler.base= "/";
		this.handler.sh = rootHandler;

	}

	//Port and Host
	public void setPort(int port) throws ConfigurationException{
		try {
			Conf.getConf().setPort(""+port);
		} catch (NotValidInputException nve) {
			throw new ConfigurationException(nve.getMessage());
		}
	}
	public void setHost(String host) throws ConfigurationException{
		try {
			Conf.getConf().setHost(host);
		} catch (NotValidInputException nve) {
			throw new ConfigurationException(nve.getMessage());
		}
	}

	/**
	 * Adds package where server can find servlets
	 *
	 * @param mainPackage package name as String
	 */
	public void setServletPackage(String mainPackage){
		if(this.handler.sh!=null){
			this.handler.sh.setMainPkg(mainPackage);
		}
	}
	/**
	 * Adds servlet to Server. In case of using custom handlers servlets will be added to latest handler created (current in spot).
	 *
	 * @param servletClass servlet class name as String
	 * @param base path this servlet maps to
	 */
	public void addServlet(String servletClass,String base){
		servlets.put(servletClass, base);
	}

	/**
	 * Gets handler from private slot and adds it to server, also annulate slot after addition
	 */
	public void commitHandler(){
		if(this.handler==null){
			log.warn("No handler to commit");
			return;
		} else {
			if(this.handler.base==null){
				log.warn("I have handler, but no base (path) was set. Make sure you correctly created handler.");
				this.handler = null;
				return;
			}
			//two cases: ServletHandler and common Handler
			if(this.handler.sh!=null){
				//adding servlets to handler
				this.prepareServlets();
				super.addServletHandler(this.handler.sh,this.handler.base);
				//clear servlet array
				this.servlets.clear();
			} else if(this.handler.h!=null){
				super.addHandler(this.handler.h, this.handler.base);
			} else {
				//none of above
				log.warn("No valid handler present. Make sure you that handler is created or added.");
			}
		}
		//finally annulate
		this.handler = null;
	}

	public void commitRootHandler(){
		this.commitHandler();
	}
	public void disableRootHandler(){
		if(this.handler!=null){
			//acting only ehen we have RootHandler
			if(this.handler.base.equals("/") && this.handler.sh.getName().equals(Defaults.ROOT_HANDLER)){
				this.handler = null;
				this.servlets.clear();
			}
		}
	}
	/**
	 * Logs warning trying to create new handler without commiting current
	 */
	private void warnHandlerExists(){
		//warning
		StringBuffer warn = new StringBuffer();
		warn.append("You are trying to create new handler without commiting previous, this will discard previously created handler."+S.lt);
		warn.append("If you did not add any handler, this caused by RootHander created by default."+S.lt);
		warn.append("Use disableRootHandler() method to discard this handler"+S.lt);
		warn.append("Or you can use this handler and add servlets to it, but after you've done use commitRootHandler() method, just before creating your new handler.");

		Shell.eecho(LOG.WARN+warn.toString()); //because log.warn() cannot use multi-line input in normal way
	}
	/**
	 * Creates new ServletHandler and adds it to handler slot.
	 *
	 * @param name Human readable name (for logging) - can be set as null.
	 * @param base Path this handler serves (cannot be set as null)
	 * @param mainPackage Package name where servlets are located. If set as null, you will need to define full Servlet class name (with package)
	 */
	public void createHandler(String name, String base, String mainPackage){
		if(this.handler!=null){
			this.warnHandlerExists();
			this.handler = null;
			this.servlets.clear();
		}
		//creating new one
		this.handler = new WebHandler();
		if(name!=null){
			this.handler.sh = new ServletHandler(name);
		} else {
			this.handler.sh = new ServletHandler();
		}

		this.handler.base= base;
		if(mainPackage!=null){ this.handler.sh.setMainPkg(mainPackage);}
	}

	/**
	 * Adds custom handler to server
	 *
	 * @param handler Handler
	 * @param base Path which handler serves
	 */
	public void addJettyHandler(Handler handler,String base){
		if(handler==null || base==null){ return; }
		super.addHandler(handler, base);
	}


	@Override
	public void addConnector(Connector connector){
		super.addConnector(connector);
	}

	public AccessLogger getAccessLogger(){
		//trying to get name from config
		String filename = Conf.getConf().getAccessLog();
		if(!filename.equals("")){
			return new AccessLogger(filename);
		} else {
			return new AccessLogger();
		}
	}

	public ErrorLogger getErrorLogger(){
		//trying to get name from config
		String filename = Conf.getConf().getErrorLog();
		if(!filename.equals("")){
			return new ErrorLogger(filename);
		} else {
			return new ErrorLogger();
		}
	}

	@Override
	public void addLogger(NCSARequestLog logger){
		super.addLogger(logger);
	}


	private void prepareServlets(){
		//adding servlets
		Iterator<String> iter = servlets.keySet().iterator();

		while(iter.hasNext()){
			String servletName = iter.next();
			String base = servlets.get(servletName);

			this.handler.sh.addServlet(servletName, base);
		}
	}
	private void makeConnector(){
		//making connector
		connector.setPort(Conf.getConf().getPort());
		connector.setHost(Conf.getConf().getHost());
	}

	@Override
	public void run() throws RunException{
		//checking for any uncommiting handlers
		if(this.handler!=null){
			this.commitHandler();
		}

		//we make connector here to allow user set host and port after init()
		this.makeConnector();
		super.addConnector(connector);

		//running
		try{
			super.run();
		}catch(Exception e){
			log.warn(e.getMessage());
			log.debug(e);
			throw new RunException(e);
		}
	}

	//Struct for Handlers
	private class WebHandler{
		String base = null;
		ServletHandler sh = null;
		Handler h = null;

		@Override
		public String toString(){
			StringBuffer sb = new StringBuffer();
			if(base!=null){ sb.append(base+S.lt); }
			if(sh != null){ sb.append(sh.getName()+S.lt); }
			if(h != null) { sb.append(h.getClass().getSimpleName()+S.lt); }
			return sb.toString();
		}
	}
}
