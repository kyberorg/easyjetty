package virtalab.easyjetty.app.exceptions;

public class CoreError extends Error {

	private static final long serialVersionUID = -2485467632247181073L;

	public CoreError() {
	}

	public CoreError(String message) {
		super(message);
	}

	public CoreError(Throwable cause) {
		super(cause);
	}

	public CoreError(String message, Throwable cause) {
		super(message, cause);
	}

}
